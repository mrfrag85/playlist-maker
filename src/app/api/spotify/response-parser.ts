/* tslint:disable:no-string-literal */
import {ISpotifyAlbum} from './interfaces/album.interface';
import {ISpotifyArtist} from './interfaces/artist.interface';
import {ISpotifyImage} from './interfaces/image.interface';
import {ISpotifyTrack} from './interfaces/track.interface';
import {ISpotifyPlaylist, ISpotifyPlaylistSnapshot} from './interfaces/playlist.interface';

export class SpotifyResponseParser {
  static parseAlbum(obj: object): ISpotifyAlbum {
    return {
      id: obj['id'],
      images: (obj['images'] || []).map(I => SpotifyResponseParser.parseImage(I)),
      name: obj['name'],
      releaseDate: obj['release_date'],
      uri: obj['uri'],
    };
  }

  static parseArtist(obj: object): ISpotifyArtist {
    return {
      id: obj['id'],
      name: obj['name'],
      uri: obj['uri'],
    };
  }

  static parseImage(obj: object): ISpotifyImage {
    return {
      height: +obj['height'],
      width: +obj['width'],
      url: obj['url'],
    };
  }

  static parseTrack(obj: object): ISpotifyTrack {
    return {
      album: this.parseAlbum(obj['album']),
      artists: (obj['artists'] || []).map(I => SpotifyResponseParser.parseArtist(I)),
      durationMs: +obj['duration_ms'] || 0,
      isExplicit: obj['explicit'] || false,
      id: obj['id'],
      name: obj['name'],
      popularity: +obj['popularity'] || 0,
      uri: obj['uri'],
    };
  }

  static parsePlaylist(obj: object): ISpotifyPlaylist {
    return {
      description: obj['description'],
      id: obj['id'],
      name: obj['name'],
      uri: obj['uri'],
      url: obj['external_urls']?.['spotify'],
      ...SpotifyResponseParser.parsePlaylistOperationResponse(obj)
    };
  }

  static parsePlaylistOperationResponse(obj: object): ISpotifyPlaylistSnapshot {
    return {
      snapshotId: obj['snapshot_id'],
    };
  }
}
