/* tslint:disable:no-string-literal */
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {SpotifyResponseParser} from '../response-parser';
import {SpotifyHttpService} from '../http/http.service';
import {ISpotifyUser} from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class SpotifyUserService {

  constructor(
    private readonly spotifyHttp: SpotifyHttpService,
  ) {
  }

  getCurrentUser(): Observable<ISpotifyUser> {
    return this.spotifyHttp.get('me').pipe(
      map(response => this.parseCurrentUserResponse(response))
    );
  }

  private parseCurrentUserResponse(obj: object): ISpotifyUser {
    return {
      displayName: obj['display_name'],
      id: obj['id'],
      images: (obj['images'] || []).map(I => SpotifyResponseParser.parseImage(I)),
      type: obj['type'],
      uri: obj['uri']
    };
  }
}
