import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SpotifyAuthService} from '../auth/auth.service';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpotifyHttpService {
  private baseUrl = environment.spotify.apiBaseUrl;

  constructor(
    private readonly ngHttpClient: HttpClient,
    private readonly authService: SpotifyAuthService,
  ) {
  }

  get(path: string, queryParams: object = {}): Observable<any> {
    return this.ngHttpClient.get(
      this.getUrl(path),
      this.getHttpOptions(queryParams)
    );
  }

  post(path: string, body: any | null, queryParams: object = {}): Observable<any> {
    return this.ngHttpClient.post(
      this.getUrl(path),
      body,
      this.getHttpOptions(queryParams)
    );
  }

  private getUrl(path: string): string {
    return `${this.baseUrl}/${path}`;
  }

  private getHttpOptions(queryParams: object): object {
    return {
      headers: {
        Authorization: this.getBearer(),
      },
      params: queryParams
    };
  }

  private getBearer(): string {
    return `Bearer ${this.authService.getAuthToken()}`;
  }
}
