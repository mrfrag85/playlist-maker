import {Injectable} from '@angular/core';
import {SpotifyHttpService} from '../http/http.service';
import {SpotifyAuthService} from '../auth/auth.service';
import {Observable} from 'rxjs';
import {exhaustMap, map} from 'rxjs/operators';
import {SpotifyResponseParser} from '../response-parser';
import {ISpotifyPlaylist, ISpotifyPlaylistSnapshot} from '../interfaces/playlist.interface';

@Injectable({
  providedIn: 'root'
})
export class SpotifyPlaylistService {

  constructor(
    private readonly spotifyAuth: SpotifyAuthService,
    private readonly spotifyHttp: SpotifyHttpService,
  ) {
  }

  /**
   * Combines createPlaylist and addItemsToPlaylist in a single function
   * Will return ISpotifyPlaylist interface with snapshotId updated to the one obtained from addItemsToPlaylist
   */
  createPlaylistWithTracks(
    name: string,
    description: string,
    trackURIs: string[]
  ): Observable<ISpotifyPlaylist> {
    return this.createPlaylist(name, description).pipe(
      exhaustMap(createResponse => this.addItemsToPlaylist(createResponse.id, trackURIs).pipe(
        map(addItemsResponse => ({
          ...createResponse,
          snapshotId: addItemsResponse.snapshotId,    // Overwrite snapshotId
        })),
      ))
    );
  }

  /**
   * Creates an playlist without tracks
   */
  createPlaylist(
    name: string,
    description: string,
  ): Observable<ISpotifyPlaylist> {
    const path = `users/${this.spotifyAuth.getUserId()}/playlists`;
    const body = {name, description, public: false};
    return this.spotifyHttp.post(path, body).pipe(
      map(R => SpotifyResponseParser.parsePlaylist(R)),
    );
  }

  /**
   * Adds items to an existing playlist
   * Returns a new snapshotId
   */
  addItemsToPlaylist(
    playlistId: string,
    trackURIs: string[]
  ): Observable<ISpotifyPlaylistSnapshot> {
    const path = `playlists/${playlistId}/tracks`;
    const body = {
      uris: trackURIs,
    };
    return this.spotifyHttp.post(path, body).pipe(
      map(R => SpotifyResponseParser.parsePlaylistOperationResponse(R)),
    );
  }
}
