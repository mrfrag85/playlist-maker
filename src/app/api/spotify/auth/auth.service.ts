import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SpotifyAuthService {
  private clientId = environment.spotify.clientId;
  private userId: string = null;
  private authToken: string = null;
  private expireTs: number;

  constructor() {
  }

  getAuthenticationUrl(
    redirectUri: string,
    scopes: string[] = [],
  ): string {
    const queryParamsStr = [
      `client_id=${this.clientId}`,
      `redirect_uri=${encodeURI(redirectUri)}`,
      `scope=${encodeURI(scopes.join(','))}`,
      'response_type=token'
    ].join('&');
    return `${environment.spotify.authorizeUrl}?${queryParamsStr}`;
  }

  authenticate(authToken: string, expiresIn: number = 3600): void {
    this.authToken = authToken;
    this.expireTs = this.currentTS() + expiresIn;
  }

  isAuthenticated(): boolean {
    return this.authToken !== null && this.expireTs > this.currentTS();
  }

  setUserId(id: string): void {
    this.userId = id;
  }

  getUserId(): string {
    return this.userId;
  }

  getAuthToken(): string {
    if (!this.isAuthenticated()) {
      throw new Error('Not Authenticated');
    }

    return this.authToken;
  }

  // noinspection JSMethodCanBeStatic
  private currentTS(): number {
    return Math.floor(Date.now() / 1000);
  }
}
