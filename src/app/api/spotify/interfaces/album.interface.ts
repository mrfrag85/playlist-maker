import {ISpotifyImage} from './image.interface';

export interface ISpotifyAlbum {
  id: string;
  images: ISpotifyImage[];
  name: string;
  releaseDate: string;
  uri: string;
}
