export interface ISpotifyPlaylist extends ISpotifyPlaylistSnapshot {
  description: string;
  id: string;
  name: string;
  uri: string;
  url: string;
}

export interface ISpotifyPlaylistSnapshot {
  snapshotId: string;
}
