export interface ISpotifyImage {
  height: number;
  width: number;
  url: string;
}
