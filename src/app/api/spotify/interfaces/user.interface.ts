import {ISpotifyImage} from './image.interface';

export interface ISpotifyUser {
  displayName: string;
  id: string;
  images: ISpotifyImage[];
  type: string;
  uri: string;
}
