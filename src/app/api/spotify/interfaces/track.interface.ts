import {ISpotifyAlbum} from './album.interface';
import {ISpotifyArtist} from './artist.interface';

export interface ISpotifyTrack {
  album: ISpotifyAlbum;
  artists: ISpotifyArtist[];
  durationMs: number;
  isExplicit: boolean;
  id: string;
  name: string;
  popularity: number;
  uri: string;
}
