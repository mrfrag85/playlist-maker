export interface ISpotifyArtist {
  id: string;
  name: string;
  uri: string;
}
