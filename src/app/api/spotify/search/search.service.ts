/* tslint:disable:no-string-literal */
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ISearchResponse} from './search-response.interface';
import {SpotifyHttpService} from '../http/http.service';
import {SpotifyResponseParser} from '../response-parser';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifySearchService {

  constructor(
    private readonly spotifyHttp: SpotifyHttpService,
  ) { }

  searchTrack(q: string): Observable<ISearchResponse> {
    return this.spotifyHttp.get('search', {
      q,
      type: 'track',
      market: 'IT',
    }).pipe(
      map(response => this.parseSearchTrackResponse(response['tracks']))
    );
  }

  private parseSearchTrackResponse(obj: object): ISearchResponse {
    return {
      items: (obj['items'] || []).map(I => SpotifyResponseParser.parseTrack(I)),
      limit: obj['limit'] || 0,
      offset: obj['offset'] || 0,
      total: obj['total'] || 0,
    };
  }
}
