import {ISpotifyTrack} from '../interfaces/track.interface';

export interface ISearchResponse {
  items: ISpotifyTrack[];
  limit: number;
  offset: number;
  total: number;
}
