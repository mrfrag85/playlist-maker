import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {IAppState} from './store/app.state';
import {selectApp} from './store/app.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  state$: Observable<IAppState>;

  constructor(
    private readonly store: Store<any>
  ) {
    this.state$ = store.pipe(select(selectApp));
  }

  ngOnInit(): void {

  }
}
