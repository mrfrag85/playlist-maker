import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {AuthComponent} from './components/auth/auth.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {StoreModule} from '@ngrx/store';
import {appReducers} from './store/app.reducers';
import {EffectsModule} from '@ngrx/effects';
import {AppEffects} from './store/app.effects';
import {RouterModule} from '@angular/router';
import {PlaylistModule} from './playlist/playlist.module';
import {MatButtonModule} from '@angular/material/button';
import {ErrorZoneComponent} from './components/error-zone/error-zone.component';
import {UserInfoComponent} from './components/user-info/user-info.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    UserInfoComponent,
    ErrorZoneComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot([]),
    BrowserAnimationsModule,
    StoreModule.forRoot({app: appReducers}, {}),
    EffectsModule.forRoot([AppEffects]),
    PlaylistModule,
    MatButtonModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
