import {ISpotifyUser} from '../api/spotify/interfaces/user.interface';

export const mockedUserInfo: ISpotifyUser = JSON.parse('{ "displayName": "Matteo Crosta", "id": "1198283421", "images": [ { "height": null, "width": null, "url": "https://scontent.flux1-1.fna.fbcdn.net/v/t1.6435-1/p320x320/54518175_2769161486435279_3763773944218255360_n.jpg?_nc_cat=111&ccb=1-3&_nc_sid=0c64ff&_nc_ohc=IPcPaFlqQa4AX915jC6&_nc_ht=scontent.flux1-1.fna&tp=6&oh=3135a1517f79176f91987848fa3e276a&oe=609DB554" } ], "type": "user", "uri": "spotify:user:1198283421" }');
