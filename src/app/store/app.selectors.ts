import {createFeatureSelector, createSelector} from '@ngrx/store';
import {IAppState} from './app.state';

export const selectApp = createFeatureSelector<any, IAppState>('app');
export const selectCurrentUser = createSelector(selectApp, (state: IAppState) => state.userInfo);
export const selectError = createSelector(selectApp, (state: IAppState) => state.error);
