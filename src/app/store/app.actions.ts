import {createAction, props, union} from '@ngrx/store';
import {ISpotifyUser} from '../api/spotify/interfaces/user.interface';

export enum AppActions {
  USER_AUTHENTICATED = '[Auth Component] User Authenticated',
  USER_INFO_LOADED = '[App] User Info Loaded',
  SHOW_ERROR = '[App] Show Error',
  DISMISS_ERROR = '[App] Dismiss Error'
}

export const userAuthenticated = createAction(AppActions.USER_AUTHENTICATED);
export const userInfoLoaded = createAction(AppActions.USER_INFO_LOADED, props<{ userInfo: ISpotifyUser }>());
export const showError = createAction(AppActions.SHOW_ERROR, props<{ message: string }>());
export const dismissError = createAction(AppActions.DISMISS_ERROR);

const all = union({
  userAuthenticated,
  userInfoLoaded,
  showError,
  dismissError,
});

export type AppActionUnion = typeof all;
