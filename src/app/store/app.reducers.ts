import {IAppState} from './app.state';
import {createReducer, on} from '@ngrx/store';
import {dismissError, showError, userAuthenticated, userInfoLoaded} from './app.actions';
import {ISpotifyUser} from '../api/spotify/interfaces/user.interface';

export const initialState: IAppState = {
  isAuthenticated: false
};

export const appReducers = createReducer(
  initialState,
  on(userAuthenticated, (state) => userAuthenticatedState(state)),
  on(userInfoLoaded, (state, {userInfo}) => userInfoLoadedState(state, userInfo)),
  on(showError, (state, {message}) => showErrorState(state, message)),
  on(dismissError, (state) => dismissErrorState(state)),
);

export function userAuthenticatedState(state: IAppState): IAppState {
  return {
    ...initialState,
    isAuthenticated: true,
  };
}

export function userInfoLoadedState(state: IAppState, userInfo: ISpotifyUser): IAppState {
  return {
    ...state,
    userInfo,
  };
}

export function showErrorState(state: IAppState, message: string): IAppState {
  return {
    ...state,
    error: {message}
  };
}

export function dismissErrorState(state: IAppState): IAppState {
  return {
    ...state,
    error: undefined
  };
}
