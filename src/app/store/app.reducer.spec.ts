import {appReducers, initialState} from './app.reducers';
import {dismissError, showError, userAuthenticated, userInfoLoaded} from './app.actions';
import {mockedUserInfo} from './_test.mocks';
import {IAppState} from './app.state';

function getInitialState(): IAppState {
  return {...initialState};
}

describe('AppReducer', () => {
  describe('Test userAuthenticated Action', () => {
    it('Should set authenticated flag', () => {
      let state = getInitialState();
      const action = userAuthenticated();

      // Execute action
      state = appReducers(state, action);

      // Check final state
      expect(state.isAuthenticated).toBeTrue();
    });
  });

  describe('Test userInfoLoaded Action', () => {
    it('Should store authenticated user info', () => {
      let state = getInitialState();
      const action = userInfoLoaded({userInfo: mockedUserInfo});

      // Execute action
      state = appReducers(state, action);

      // Check final state
      expect(state.userInfo).toBeDefined();
      expect(state.userInfo).toEqual(mockedUserInfo);
    });
  });

  describe('Test showError Action', () => {
    it('Should set an error message', () => {
      let state = getInitialState();
      const action = showError({message: 'LOOK AT ME, I\'M AN ERROR!'});

      // Execute action
      state = appReducers(state, action);

      // Check final state
      expect(state.error).toBeDefined();
      expect(state.error.message).toBeDefined();
      expect(state.error.message).toEqual('LOOK AT ME, I\'M AN ERROR!');
    });
  });

  describe('Test dismissError Action', () => {
    it('Should clear all errors', () => {
      let state = getInitialState();
      state.error = {message: 'TEST ERROR'};
      const action = dismissError();

      // Execute action
      state = appReducers(state, action);

      // Check final state
      expect(state.error).toBeUndefined();
    });
  });
});
