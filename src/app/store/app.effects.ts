import {Actions, createEffect, ofType} from '@ngrx/effects';
import {Injectable} from '@angular/core';
import {IAppState} from './app.state';
import {Store} from '@ngrx/store';
import {AppActions, AppActionUnion, showError, userInfoLoaded} from './app.actions';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {SpotifyUserService} from '../api/spotify/user/user.service';
import {of} from 'rxjs';
import {SpotifyAuthService} from '../api/spotify/auth/auth.service';

@Injectable()
export class AppEffects {

  /**
   * On USER_AUTHENTICATED action:
   *  call getCurrentUser and wait for a response
   *    -> if everything is fine set user id on spotifyAuthService and dispatch userInfoLoaded
   *    -> if something was wrong show the error
   */
  loadUserInfo$ = createEffect(() => this.actions$.pipe(
    ofType(AppActions.USER_AUTHENTICATED),
    mergeMap(() => this.spotifyUserService.getCurrentUser().pipe(
      map(userInfo => {
        this.spotifyAuthService.setUserId(userInfo.id);
        return userInfoLoaded({userInfo})
      }),
      catchError((error) => of(showError({message: error.message})))
    ))
  ));

  constructor(
    private readonly store$: Store<IAppState>,
    private readonly actions$: Actions<AppActionUnion>,
    private readonly spotifyAuthService: SpotifyAuthService,
    private readonly spotifyUserService: SpotifyUserService,
  ) {
  }
}
