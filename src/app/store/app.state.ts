import {ISpotifyUser} from '../api/spotify/interfaces/user.interface';

export interface IAppState {
  isAuthenticated: boolean;
  userInfo?: ISpotifyUser;
  error?: IAppError;
}

export interface IAppError {
  message: string;
}
