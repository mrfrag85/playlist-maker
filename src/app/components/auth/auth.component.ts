import {Component, OnDestroy, OnInit} from '@angular/core';
import {SpotifyAuthService} from '../../api/spotify/auth/auth.service';
import {ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import {showError, userAuthenticated} from '../../store/app.actions';
import {environment} from '../../../environments/environment';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.sass']
})
export class AuthComponent implements OnInit, OnDestroy {
  loginUrl: string;
  destory$ = new Subject<void>();

  constructor(
    private readonly store: Store,
    private readonly route: ActivatedRoute,
    private readonly authService: SpotifyAuthService,
  ) {
  }

  ngOnInit(): void {
    this.loginUrl = this.authService.getAuthenticationUrl(
      environment.appUrl,
      [...environment.spotify.requiredScopes]
    );

    this.route.fragment.pipe(
      takeUntil(this.destory$)  // Unsubscribe on destroy
    ).subscribe((fragment: string) => {
      const params = new URLSearchParams(fragment);

      if (params.get('access_token')) {
        // Login OK
        this.authService.authenticate(params.get('access_token'), +params.get('expires_in'));
        this.store.dispatch(userAuthenticated());
      } else if (params.get('error')) {
        // Login failed
        const message = `Authentication failed: ${params.get('error')}`;
        this.store.dispatch(showError({message}));
      }
    });
  }

  ngOnDestroy(): void {
    this.destory$.next();
  }
}
