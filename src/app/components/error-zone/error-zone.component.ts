import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {IAppError} from '../../store/app.state';
import {select, Store} from '@ngrx/store';
import {selectError} from '../../store/app.selectors';

@Component({
  selector: 'app-error-zone',
  templateUrl: './error-zone.component.html',
  styleUrls: ['./error-zone.component.sass']
})
export class ErrorZoneComponent implements OnInit {
  error$: Observable<IAppError>;

  constructor(
    private readonly store: Store<any>
  ) {
    this.error$ = store.pipe(select(selectError));
  }

  ngOnInit(): void {
  }

}
