import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {ISpotifyUser} from '../../api/spotify/interfaces/user.interface';
import {selectCurrentUser} from '../../store/app.selectors';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.sass']
})
export class UserInfoComponent implements OnInit {
  userInfo$: Observable<ISpotifyUser>;

  constructor(
    private readonly store: Store,
  ) {
    this.userInfo$ = this.store.pipe(select(selectCurrentUser));
  }

  ngOnInit(): void {
  }

}
