import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlaylistComponent} from './playlist.component';
import {StoreModule} from '@ngrx/store';
import * as PlaylistReducers from './store/playlist.reducers';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {TrackAutocompleteComponent} from './components/track-autocomplete/track-autocomplete.component';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {ArtistsPipe} from './pipes/artists.pipe';
import {MatIconModule} from '@angular/material/icon';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {EffectsModule} from '@ngrx/effects';
import {PlaylistEffects} from './store/playlist.effects';
import {ExportOkComponent} from './components/export-ok/export-ok.component';


@NgModule({
  declarations: [
    PlaylistComponent,
    TrackAutocompleteComponent,
    ArtistsPipe,
    ExportOkComponent,
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature('playlist', PlaylistReducers.reducer),
    EffectsModule.forFeature([PlaylistEffects]),
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    MatListModule,
    MatIconModule,
    DragDropModule,
    MatButtonModule,
    MatProgressSpinnerModule,
  ],
  exports: [
    PlaylistComponent,
  ]
})
export class PlaylistModule {
}
