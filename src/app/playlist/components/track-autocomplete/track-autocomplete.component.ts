import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable, of} from 'rxjs';
import {ISpotifyTrack} from '../../../api/spotify/interfaces/track.interface';
import {SpotifySearchService} from '../../../api/spotify/search/search.service';
import {debounceTime, map, switchMap} from 'rxjs/operators';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';

@Component({
  selector: 'app-track-autocomplete',
  templateUrl: './track-autocomplete.component.html',
  styleUrls: ['./track-autocomplete.component.sass']
})
export class TrackAutocompleteComponent implements OnInit {
  funnyTrackPlaceholder = 'Titolo della canzone';
  autocompleteControl = new FormControl();
  options$: Observable<ISpotifyTrack[]>;

  @Output() trackSelected = new EventEmitter<ISpotifyTrack>();

  constructor(
    private readonly spotifySearchService: SpotifySearchService,
  ) {
  }

  ngOnInit(): void {
    this.options$ = this.autocompleteControl.valueChanges.pipe(
      debounceTime(250),
      switchMap(q => {
        if (!q || q === '') {
          return of([]);
        }
        return this.spotifySearchService.searchTrack(q).pipe(
          map(response => response.items)
        );
      })
    );
  }

  optionSelected(evt: MatAutocompleteSelectedEvent): void {
    this.trackSelected.emit(evt.option.value);
    this.autocompleteControl.setValue(null);
  }

}
