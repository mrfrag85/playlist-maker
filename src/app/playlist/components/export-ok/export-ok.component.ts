import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {IPlaylistState} from '../../store/playlist.state';
import {select, Store} from '@ngrx/store';
import {selectPlaylist} from '../../store/playlist.selectors';
import {initialize} from '../../store/playlist.actions';

@Component({
  selector: 'app-export-ok',
  templateUrl: './export-ok.component.html',
  styleUrls: ['./export-ok.component.sass']
})
export class ExportOkComponent implements OnInit {
  playlistState$: Observable<IPlaylistState>;

  constructor(
    private readonly store: Store,
  ) {
    this.playlistState$ = this.store.pipe(select(selectPlaylist));
  }

  ngOnInit(): void {
  }

  newPlaylist(): void {
    this.store.dispatch(initialize());
  }
}
