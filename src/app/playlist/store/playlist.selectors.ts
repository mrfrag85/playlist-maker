import {createFeatureSelector} from '@ngrx/store';
import {IPlaylistState} from './playlist.state';

export const selectPlaylist = createFeatureSelector<any, IPlaylistState>('playlist');
