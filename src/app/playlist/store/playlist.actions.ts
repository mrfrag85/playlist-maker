import {createAction, props, union} from '@ngrx/store';
import {ISpotifyTrack} from '../../api/spotify/interfaces/track.interface';
import {ISpotifyPlaylist} from '../../api/spotify/interfaces/playlist.interface';

export enum PlaylistActions {
  INITIALIZE = '[Playlist] Initialize',
  SET_TITLE = '[Playlist] SetTitle',
  ADD_TRACK = '[Playlist] Add Track',
  REMOVE_TRACK_AT_POSITION = '[Playlist] Remove Track At Position',
  MOVE_TRACK_AT_POSITION = '[Playlist] Move Track At Position',
  START_EXPORT = '[Playlist] Start export',
  EXPORT_OK = '[Playlist] Export ok',
  EXPORT_FAIL = '[Playlist] Export fail',
}

export const initialize = createAction(PlaylistActions.INITIALIZE);
export const setTitle = createAction(PlaylistActions.SET_TITLE, props<{ title: string }>());
export const addTrack = createAction(PlaylistActions.ADD_TRACK, props<{ track: ISpotifyTrack }>());
export const removeTrackAtPosition = createAction(PlaylistActions.REMOVE_TRACK_AT_POSITION, props<{ position: number }>());
export const moveTrackAtPosition = createAction(PlaylistActions.MOVE_TRACK_AT_POSITION, props<{ oldPosition: number, newPosition: number }>());
export const startExport = createAction(PlaylistActions.START_EXPORT);
export const exportOk = createAction(PlaylistActions.EXPORT_OK, props<{ playlist: ISpotifyPlaylist }>());
export const exportFail = createAction(PlaylistActions.EXPORT_FAIL, props<{ reason: string }>());

const all = union({
  initialize,
  setTitle,
  addTrack,
  removeTrackAtPosition,
  moveTrackAtPosition,
  startExport,
  exportOk,
  exportFail
});

export type PlaylistActionUnion = typeof all;
