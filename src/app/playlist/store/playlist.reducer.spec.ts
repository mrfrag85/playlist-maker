import {
  addTrack,
  exportOk,
  initialize,
  moveTrackAtPosition,
  removeTrackAtPosition,
  setTitle,
  startExport
} from './playlist.actions';
import {initialState, playlistReducers} from './playlist.reducers';
import {IPlaylistState} from './playlist.state';
import {mockedPlaylist, mockedTracks} from './_test.mocks';

function getInitialState(): IPlaylistState {
  return {...initialState};
}

describe('Playlist Reducer', () => {
  describe('Test initialize Action', () => {
    it('Should reset to initial state', () => {
      let state: IPlaylistState = {
        title: 'Test title',
        tracks: mockedTracks,
        canBeExported: true,
        hasBeenExported: true,
      };
      const action = initialize();

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.title).toEqual('');
      expect(state.tracks).toEqual([]);
      expect(state.canBeExported).toBeFalse();
    });
  });

  describe('Test setTitle Action', () => {
    it('Should set the title', () => {
      let state = getInitialState();
      const action = setTitle({title: 'Test Title'});

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.title).toEqual('Test Title');
    });

    it('Should keep canExport false when there are no tracks', () => {
      let state = getInitialState();
      const action = setTitle({title: 'Test Title'});

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.canBeExported).toBeFalse();
    });

    it('Should enable canExport when there are tracks', () => {
      let state = getInitialState();
      state.tracks = mockedTracks;
      const action = setTitle({title: 'Test Title'});

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.canBeExported).toBeTrue();
    });

    it('Should disable canExport when title is empty', () => {
      let state = getInitialState();
      state.title = 'TEST TITLE';
      state.tracks = mockedTracks;
      const action = setTitle({title: ''});

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.canBeExported).toBeFalse();
    });
  });

  describe('Test addTrack action', () => {
    it('Should add a track', () => {
      let state = getInitialState();
      const action = addTrack({track: mockedTracks[0]});

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.tracks.length).toBe(1);
      expect(state.tracks[0].name).toBe('Redemption Song');
    });

    it('Should enable canExport when a title is set', () => {
      let state = getInitialState();
      state.title = 'TEST';
      const action = addTrack({track: mockedTracks[0]});

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.canBeExported).toBeTrue();
    });

    it('Should keep canExport disabled when a title is not set', () => {
      let state = getInitialState();
      const action = addTrack({track: mockedTracks[0]});

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.canBeExported).toBeFalse();
    });
  });

  describe('Test removeTrackAtPosition action', () => {
    it('Should remove a track', () => {
      let state = getInitialState();
      state.tracks = mockedTracks;
      const action = removeTrackAtPosition({position: 0});

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.tracks.length).toBe(4);
      expect(state.tracks[0].name).toBe('Comfortably Numb - 2011 Remastered Version');
    });

    it('Should disable canExport when the last track has been removed', () => {
      let state = getInitialState();
      state.tracks = [mockedTracks[0]];
      state.title = 'TEST';
      const action = removeTrackAtPosition({position: 0});

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.tracks.length).toBe(0);
      expect(state.canBeExported).toBeFalse();
    });
  });

  describe('Test moveTrackAtPosition action', () => {
    it('Should switch two tracks', () => {
      let state = getInitialState();
      state.tracks = mockedTracks;
      const action = moveTrackAtPosition({oldPosition: 1, newPosition: 0});

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.tracks.length).toBe(5);
      expect(state.tracks[0].name).toBe('Comfortably Numb - 2011 Remastered Version');
      expect(state.tracks[1].name).toBe('Redemption Song');
    });
  });

  describe('Test startExport action', () => {
    it('Should.. well, do nothing', () => {
      let state = getInitialState();
      state.title = 'TEST TITLE';
      state.tracks = mockedTracks;
      const action = startExport();

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state).toEqual(state);
    });
  });

  describe('Test exportOk action', () => {
    it('Should.. well, do nothing', () => {
      let state = getInitialState();
      state.title = 'TEST PLAYLIST';
      state.tracks = mockedTracks;
      const action = exportOk({playlist: mockedPlaylist});

      // Execute action
      state = playlistReducers(state, action);

      // Check final state
      expect(state.hasBeenExported).toBeTrue();
      expect(state.exportedPlaylistUrl).toEqual('https://open.spotify.com/playlist/3MYnI37XfQ3qLsu4p0HVcC');
    });
  });
});
