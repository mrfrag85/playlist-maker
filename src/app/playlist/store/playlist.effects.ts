import {Actions, createEffect, ofType} from '@ngrx/effects';
import {catchError, concatMap, exhaustMap, map, withLatestFrom} from 'rxjs/operators';
import {of} from 'rxjs';
import {Injectable} from '@angular/core';
import {SpotifyPlaylistService} from '../../api/spotify/playlist/playlist.service';
import {exportOk, PlaylistActions, PlaylistActionUnion} from './playlist.actions';
import {IPlaylistState} from './playlist.state';
import {select, Store} from '@ngrx/store';
import {selectPlaylist} from './playlist.selectors';
import {showError} from '../../store/app.actions';

@Injectable()
export class PlaylistEffects {

  /**
   * On START_EXPORT action:
   *  get latest playlist state
   * THEN
   *  call createPlaylistWithTracks and wait for a response
   *    -> if everything is fine dispatch exportOk
   *    -> if something was wrong dispatch showError
   */
  startExport$ = createEffect(() => this.actions$.pipe(
    ofType(PlaylistActions.START_EXPORT),

    concatMap(action => of(action).pipe(
      withLatestFrom(this.store.pipe(select(selectPlaylist)))
    )),

    exhaustMap(([action, state]) => {
      const trackURIS = state.tracks.map(T => T.uri);
      return this.playlistService.createPlaylistWithTracks(state.title, '', trackURIS).pipe(
        map(playlist => exportOk({playlist})),
        catchError((error) => of(showError({message: error.message})))
      );
    })
  ));

  constructor(
    private readonly store: Store<IPlaylistState>,
    private readonly actions$: Actions<PlaylistActionUnion>,
    private readonly playlistService: SpotifyPlaylistService
  ) {
  }
}
