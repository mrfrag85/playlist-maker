import {Action, createReducer, on} from '@ngrx/store';
import {IPlaylistState} from './playlist.state';
import {
  addTrack,
  exportOk,
  initialize,
  moveTrackAtPosition,
  removeTrackAtPosition,
  setTitle,
  startExport
} from './playlist.actions';
import {ISpotifyTrack} from '../../api/spotify/interfaces/track.interface';
import {ISpotifyPlaylist} from '../../api/spotify/interfaces/playlist.interface';

export const initialState: IPlaylistState = {
  title: '',
  tracks: [],
  canBeExported: false,
  hasBeenExported: false,
};

export const playlistReducers = createReducer(
  initialState,
  on(initialize, () => initialState),
  on(setTitle, (state, {title}) => setTitleState(state, title)),
  on(addTrack, (state, {track}) => addTrackState(state, track)),
  on(removeTrackAtPosition, (state, {position}) => removeTrackAtPositionState(state, position)),
  on(moveTrackAtPosition, (state, {
    oldPosition,
    newPosition
  }) => moveTrackAtPositionState(state, oldPosition, newPosition)),
  on(startExport, (state) => startExportState(state)),
  on(exportOk, (state, {playlist}) => exportOkState(state, playlist)),
);

/**
 * EXPORT
 */

export function reducer(state, action: Action): IPlaylistState {
  return playlistReducers(state, action);
}

/**
 * STATE REDUCERS
 */

function setTitleState(state: IPlaylistState, title: string): IPlaylistState {
  return setCanBeExportedFlag({...state, title});
}

function addTrackState(state: IPlaylistState, track: ISpotifyTrack): IPlaylistState {
  const tracks = [...state.tracks, track];
  return setCanBeExportedFlag({...state, tracks});
}

function removeTrackAtPositionState(state: IPlaylistState, position: number): IPlaylistState {
  const tracks = [...state.tracks];
  tracks.splice(position, 1);
  return setCanBeExportedFlag({...state, tracks});
}

function moveTrackAtPositionState(state: IPlaylistState, oldPosition: number, newPosition: number): IPlaylistState {
  const movingTrack = state.tracks[oldPosition];
  const tracks = [...state.tracks];
  tracks.splice(oldPosition, 1);
  tracks.splice(newPosition, 0, movingTrack);
  return {...state, tracks};
}

function startExportState(state: IPlaylistState): IPlaylistState {
  return {
    ...state,
  };
}

function exportOkState(state: IPlaylistState, playlist: ISpotifyPlaylist): IPlaylistState {
  return {
    ...state,
    hasBeenExported: true,
    exportedPlaylistUrl: playlist.url,
  };
}

/**
 * UTILITIES
 */

function setCanBeExportedFlag(state: IPlaylistState): IPlaylistState {
  return {
    ...state,
    canBeExported: (state.tracks.length > 0) && state.title.length > 0,
  };
}



