import {ISpotifyTrack} from '../../api/spotify/interfaces/track.interface';

export interface IPlaylistState {
  title: string;
  tracks: ISpotifyTrack[];
  canBeExported: boolean;
  hasBeenExported: boolean;
  exportedPlaylistUrl?: string;
}
