import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {IPlaylistState} from './store/playlist.state';
import {selectPlaylist} from './store/playlist.selectors';
import {ISpotifyTrack} from '../api/spotify/interfaces/track.interface';
import {addTrack, moveTrackAtPosition, removeTrackAtPosition, setTitle, startExport} from './store/playlist.actions';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.sass']
})
export class PlaylistComponent implements OnInit {
  playlistControl = new FormControl();
  playlistState$: Observable<IPlaylistState>;

  constructor(
    private readonly store: Store,
  ) {
    this.playlistState$ = this.store.pipe(select(selectPlaylist));
  }

  ngOnInit(): void {
    this.playlistControl.valueChanges.subscribe((title) => {
      if (title && title !== '') {
        this.store.dispatch(setTitle({title}));
      }
    });
  }

  addTrack(track: ISpotifyTrack): void {
    this.store.dispatch(addTrack({track}));
  }

  removeTrackAtPosition(position: number): void {
    this.store.dispatch(removeTrackAtPosition({position}));
  }

  itemDropped(evt: { currentIndex: number, previousIndex: number }): void {
    this.store.dispatch(moveTrackAtPosition({oldPosition: evt.previousIndex, newPosition: evt.currentIndex}));
  }

  exportPlaylist(): void {
    this.store.dispatch(startExport());
    this.playlistControl.setValue('');
  }
}
