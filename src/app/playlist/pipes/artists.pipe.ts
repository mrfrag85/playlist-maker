import {Pipe, PipeTransform} from '@angular/core';
import {ISpotifyArtist} from '../../api/spotify/interfaces/artist.interface';

@Pipe({
  name: 'artists'
})
export class ArtistsPipe implements PipeTransform {

  transform(value: ISpotifyArtist | ISpotifyArtist[]): string {
    if (!Array.isArray(value)) {
      return value.name;
    }
    return value.map(artist => artist.name).join(', ');
  }

}
