export const environment = {
  production: true,
  appUrl: 'https://goofy-borg-5ba5b5.netlify.app/',
  spotify: {
    clientId: 'd50f17d7d80340b384d93f158a683677',
    authorizeUrl: 'https://accounts.spotify.com/authorize',
    apiBaseUrl: 'https://api.spotify.com/v1',
    requiredScopes: ['playlist-modify-private'],
  }
};
